<?php
// Application middleware


//MySQL connection
$app->add(function ($request, $response, $next) {
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "Practica_M06UF2_Chat";
  // Try to create connection
  try{
    $conn = new mysqli($servername, $username, $password, $dbname);
  }
  catch(Exception $e){
    $data = array("state"=>"ERROR","result"=>"Error creating connection");
    $response->withJson($data);
  }
  //Check if connection has errors
  if($conn->connect_error){
    $data = array("states"=>"ERROR","result"=>"Connection failed: $conn->connect_error");
    $response->withJson($data);
  } else {
    $request = $request->withAttribute('conn', $conn);
    $response = $next($request, $response);
    $conn->close();
  }
  return $response;
});
