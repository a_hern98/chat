<?php

use Slim\Http\Request;
use Slim\Http\Response;

function test_input($conn,$data) {
  $data = mysqli_real_escape_string($conn,$data);
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

// Routes
$app->get('/', function (Request $request, Response $response) {
    //echo "<br>Hello world!<br>";
    $data = array("message"=>"Hello world!");
    $response = $response->withJson($data);
    return $response;
});

$app->get('/getId/{name}', function (Request $request, Response $response, array $args) {
  $conn = $request->getAttribute('conn');

  $sql = "SELECT id FROM User WHERE name = ?";
  if($stmt = $conn->prepare($sql)){
    $stmt->bind_param("s", $args['name']);
    $stmt->execute();
    $stmt->bind_result($id_sql);
    $id = -1;
    while($stmt->fetch()){
      $id = $id_sql;
    }
    if($id != -1){
      $data = array("state"=>"OK","result"=>$id);
    } else {
      $data = array("state"=>"ERROR","result"=>"Id not found");
    }
    $stmt->close();
  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting de Id");
  }
  $response = $response->withJson($data);
  return $response;
});

$app->get('/checkPassword/{id}/{pwd}', function (Request $request, Response $response, array $args) {
  $conn = $request->getAttribute('conn');

  $sql = "SELECT id, password FROM User WHERE id = ? AND password = ?";
  if($stmt = $conn->prepare($sql)){
    $stmt->bind_param("is", $args['id'], $args['pwd']);
    $stmt->execute();
    $stmt->bind_result($id_sql, $pwd);
    $id = -1;
    while($stmt->fetch()){
      $id = $id_sql;
    }
    if($id != -1){
      $data = array("state"=>"OK","result"=>$id);
    } else {
      $data = array("state"=>"ERROR","result"=>"Id and password not found");
    }
    $stmt->close();
  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting de Id");
  }
  $response = $response->withJson($data);
  return $response;
});

$app->get('/getUsersByName/{name}', function (Request $request, Response $response, array $args) {
  $conn = $request->getAttribute('conn');

  $sql = "SELECT id, name FROM User WHERE name LIKE ?";
  if($stmt = $conn->prepare($sql)){
	$name = $args['name'].'%';
    $stmt->bind_param("s", $name);
    $stmt->execute();
    $stmt->bind_result($id_sql, $name_sql);
    $users = array();
    while($stmt->fetch()){
      $user = array("id"=>$id_sql, "name"=>$name_sql);
	  array_push($users, $user);
    }
    if(!empty($users)){
      $data = array("state"=>"OK","result"=>$users);
    } else {
      $data = array("state"=>"ERROR","result"=>"Users not found");
    }
    $stmt->close();
  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting de users");
  }
  $response = $response->withJson($data);
  return $response;
});

$app->post('/sendMessage', function(Request $request, Response $response){
  $conn = $request->getAttribute('conn');

  if (!empty($_POST['sender']) && !empty($_POST['receiver']) && !empty($_POST['message'])){
    $sender = test_input($conn, $_POST['sender']);
    $receiver = test_input($conn, $_POST['receiver']);
    $message = test_input($conn, $_POST['message']);

    $stmt = $conn->prepare("INSERT INTO Chat (sender, receiver, message)
                            VALUES (?, ?, ?)");
    $stmt->bind_param("iis", $sender, $receiver, $message);
    if ($stmt->execute() === TRUE) {
      $data = array("state"=>"OK","result"=>"Message sent correctly");
    } else {
      $data = array("state"=>"ERROR","result"=>"Error sending message");
    }
    $stmt->close();
  } else {
    $data = array("state"=>"ERROR","result"=>"Error: some mandatory fields are missing");
  }
  $response = $response->withJson($data);
  return $response;
});

$app->get('/getChat/{sender}/{receiver}', function(Request $request, Response $response, array $args){
  $conn = $request->getAttribute('conn');

  $sql = "SELECT sender,receiver,message,date_sended,seen FROM Chat
          WHERE (sender = ? AND receiver = ?) OR (receiver = ? AND sender = ?)";
  if($stmt = $conn->prepare($sql)){
    $stmt->bind_param("iiii", $args['sender'], $args['receiver'], $args['sender'], $args['receiver']);
    $stmt->execute();
    $stmt->bind_result($sender, $receiver, $message, $date, $seen);
    $msgs = array();
    while($stmt->fetch()){
      $msg = array("sender"=>$sender,"receiver"=>$receiver,"message"=>$message,"date"=>$date,"seen"=>$seen);
      array_push($msgs, $msg);
    }
    $stmt->close();
    $sql = "UPDATE Chat SET seen=1 WHERE sender=? AND receiver=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ii", $args['receiver'], $args['sender']);
    if ($stmt->execute()) {
        $data = array("state"=>"OK","result"=>$msgs);
    } else {
        $data = array("state"=>"ERROR","result"=>$conn->error);
    }

  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting messages");
  }
  $response = $response->withJson($data);
  return $response;
});

$app->get('/getAllResumeChats/{user}', function(Request $request, Response $response, array $args){
  $conn = $request->getAttribute('conn');

  $sql = "SELECT c.sender,
			(SELECT name FROM User AS u
			WHERE u.id=c.sender) AS sender_name,
			c.receiver,
			(SELECT name FROM User AS u
			WHERE u.id=c.receiver) AS receiver_name,
			c.message,c.date_sended,c.seen
		FROM Chat AS c
		WHERE (c.sender = ? OR c.receiver = ?)
			AND date_sended = (
				SELECT MAX(date_sended)
				FROM `Chat` AS c2
				WHERE (c.sender = c2.sender AND c.receiver = c2.receiver)
					OR (c.sender = c2.receiver AND c.receiver = c2.sender))";

  if($stmt = $conn->prepare($sql)){
    $stmt->bind_param("ii", $args['user'], $args['user']);
    $stmt->execute();
    $stmt->bind_result($sender, $sname, $receiver, $rname, $message, $date,$seen);
    $msgs = array();
    while($stmt->fetch()){
      $msg = array("sender"=>$sender,"sender_name"=>$sname,"receiver"=>$receiver,"receiver_name"=>$rname,"message"=>$message,"date"=>$date,"seen"=>$seen);
      array_push($msgs, $msg);
    }
    $data = array("state"=>"OK","result"=>$msgs);
    $stmt->close();
  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting messages");
  }
  $response = $response->withJson($data);
  return $response;
});

$app->get('/getAllUsers', function(Request $request, Response $response, array $args){
  $conn = $request->getAttribute('conn');

  $sql = "SELECT id,name FROM User";

  if($stmt = $conn->prepare($sql)){
    $stmt->execute();
    $stmt->bind_result($id, $name);
    $users = array();
    while($stmt->fetch()){
      $user = array("id"=>$id,"name"=>$name);
      array_push($users, $user);
    }
    $data = array("state"=>"OK","result"=>$users);
    $stmt->close();
  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting users");
  }
  $response = $response->withJson($data);
  return $response;
});



$app->get('/getChatNotSeen/{sender}/{receiver}', function(Request $request, Response $response, array $args){
  $conn = $request->getAttribute('conn');

  $sql = "SELECT sender,receiver,message,date_sended,seen FROM Chat
          WHERE seen = 0
            AND receiver = ? AND sender = ?";
  if($stmt = $conn->prepare($sql)){
    $stmt->bind_param("ii", $args['sender'], $args['receiver']);
    $stmt->execute();
    $stmt->bind_result($sender, $receiver, $message, $date, $seen);
    $msgs = array();
    while($stmt->fetch()){
      $msg = array("sender"=>$sender,"receiver"=>$receiver,"message"=>$message,"date"=>$date,"seen"=>$seen);
      array_push($msgs, $msg);
    }
    $stmt->close();
    $sql = "UPDATE Chat SET seen=1 WHERE sender=? AND receiver=?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("ii", $args['receiver'], $args['sender']);
    if ($stmt->execute()) {
        $data = array("state"=>"OK","result"=>$msgs);
    } else {
        $data = array("state"=>"ERROR","result"=>$conn->error);
    }

  } else {
    $data = array("state"=>"ERROR","result"=>"Error getting messages");
  }
  $response = $response->withJson($data);
  return $response;
});
