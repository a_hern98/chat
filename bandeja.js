$(document).ready(function() {
  $('#search').click(function() {
    $(location).attr('href', "buscador.html");
  });
  $.get("./api_chat/public/getAllResumeChats/" + Cookies.get('id'), function(data, status) {
    if (data.result.length === 0) {
      $('body').append('<h2 class="zeromensajes">NO HAY COMVERSACIONES<br>INICIA UNA CONVERSA "+"<h2>');
    } else {
      for (var i = 0; i < data.result.length; i++) {
        if (data.result[i].receiver == Cookies.get('id')) {
          $('body').append('<div id="' + [i] + '" class="conver"><h2 class="conver"><strong>' +
            data.result[i].sender_name + '</strong></h2><p class="conver"><img src="reciber.png"> ' + data.result[i].message + '</p>');
        } else {
          $('body').append('<div id="' + [i] + '" class="conver"><h2 class="conver"><strong>' +
            data.result[i].receiver_name + '</strong></h2><p class="conver"><img src="send.png"> ' + data.result[i].message + '</p>');
        }
      }
    }
    $("div.conver").click(function() {
      var chat = $(this).attr("id");
      Cookies.set('sender', data.result[chat].sender);
      Cookies.set('receiver', data.result[chat].receiver);
      $(location).attr('href', "chat.html");
    });
  });
});
