$(document).ready(function() {
  $('#return').click(function() {
    $(location).attr('href', "bandeja.html");
  });
  $('button').click(function() {
    let reciver
    if (Cookies.get('receiver') == Cookies.get('id')) {
      reciver=Cookies.get('sender')
    } else {
      reciver=Cookies.get('receiver')
    }
    $.post("./api_chat/public/sendMessage", {
        'sender': Cookies.get('id'),
        'receiver': reciver,
        'message': $("input").val()
      },
      function(data, status) {
        $(location).attr('href', "chat.html");
      }
    );
  });
  $.get("./api_chat/public/getAllUsers", function(data, status) {
    for (var i = 0; i < data.result.length; i++) {
      if (data.result[i].id != Cookies.get('id') && (data.result[i].id == Cookies.get('sender') || data.result[i].id == Cookies.get('receiver'))) {
        $("#header").append('<h2>' + data.result[i].name + '</h2>')
      }
    }
  });
  setInterval(actualizar(), 1000);
});
function actualizar() {
  $("#message").remove();
  $.get("./api_chat/public/getChat/" + Cookies.get('sender') + "/" + Cookies.get('receiver'), function(data, status) {
    for (var i = 0; i < data.result.length; i++) {
      if (data.result[i].sender != Cookies.get('id')) {
        $("#conversacion").append('<div id="message" class="recib">' + data.result[i].message + '</div><br></br><br></br>')
      } else {
        $("#conversacion").append('<div id="message" class="send">' + data.result[i].message + '</div><br></br><br></br>')
      }
    }
  });
}
