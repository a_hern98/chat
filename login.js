function entra() {
  $(location).attr('href', "bandeja.html");
}
$(document).ready(function() {
  $("button").click(function() {
    if ($("#name").val() != '' && $("#password").val() != '') {
      $.get("./api_chat/public/getId/" + $("#name").val(), function(data, status) {
        if (data.state == "OK") {
          Cookies.set('id', data.result);
          $.get("./api_chat/public/checkPassword/" + Cookies.get('id') + "/" + $("#password").val(), function(data, status) {
            if (data.state == "OK") {
              $('body').append('<div class="success"><strong>SUCCESS</strong></div>');
              $(".success").fadeIn(4000);
              setTimeout("entra()", 1000);
            } else if (data.state == "ERROR") {
              $('body').append('<div class="error"><strong>EL USUARIO I/O NO COINCIDEN</strong></div>');
              $(".error").fadeIn("5000");
              $("*").click(function() {
                $("div.error").remove();
              });
            }
          });
        } else if (data.state == "ERROR") {
          $('body').append('<div class="error"><strong>EL USUARIO NO EXISTE</strong></div>');
          $(".error").fadeIn("5000");
          $("*").click(function() {
            $("div.error").remove();
          });
        }
      });
    }
  });
});