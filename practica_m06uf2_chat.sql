CREATE DATABASE IF NOT EXISTS Practica_M06UF2_Chat;
USE Practica_M06UF2_Chat;
-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-12-2018 a las 18:05:10
-- Versión del servidor: 10.1.16-MariaDB
-- Versión de PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `practica_m06uf2_chat`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
`id` int(11) NOT NULL,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `message` text NOT NULL,
  `date_sended` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visto` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `chat`
--

INSERT INTO `chat` (`id`, `sender`, `receiver`, `message`, `date_sended`, `visto`) VALUES
(7, 1, 2, 'Hola!', '2018-11-20 19:16:32', 0),
(8, 2, 1, 'que tal?', '2018-11-20 19:17:24', 0),
(9, 1, 2, 'ufff fins als pebrots de ruby', '2018-11-20 19:18:24', 0),
(10, 2, 1, 'que dius pero si ruby mola!', '2018-11-20 19:18:38', 0),
(11, 2, 3, 'Hola caracola!', '2018-11-06 19:19:14', 0),
(12, 3, 2, 'Hola!', '2018-11-17 19:19:28', 0),
(13, 3, 4, 'Hola!', '2018-11-19 19:19:35', 0),
(14, 1, 2, 'estem fent una prova', '2018-11-28 15:31:10', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `Password` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `Password`) VALUES
(1, 'Adria', 'Adria'),
(2, 'Raul', 'Raul'),
(3, 'David B', 'David B'),
(4, 'Eric', 'Eric'),
(5, 'Ruben', 'Ruben'),
(6, 'Alejandro', 'Alejandro'),
(7, 'Oriol', 'Oriol'),
(8, 'Dani', 'Dani'),
(9, 'Ian', 'Ian'),
(10, 'David N', 'David N'),
(11, 'Guillem', 'Guillem'),
(12, 'Xavi', 'Xavi'),
(13, 'Marc', 'Marc'),
(14, 'Valenti', 'Valenti'),
(15, 'Edu', 'Edu'),
(16, 'Cristian', 'Cristian'),
(17, 'Carlota', 'Carlota');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender` (`sender`),
  ADD KEY `receiver` (`receiver`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `fk_receiver` FOREIGN KEY (`receiver`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_sender` FOREIGN KEY (`sender`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
